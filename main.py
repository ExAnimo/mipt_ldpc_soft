#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from collections import namedtuple
import argparse
import os
import os.path

# 1. Reading the matrix

def read_H(filename, Z):
    '''
    Reads BG and returns corresponding parity-check H. Lifting value Z
    must be supplied. See:
    Nguyen, Tram & Nguyen, Tuy & Lee, Hanho. (2019).
        Efficient QC-LDPC Encoder for 5G New Radio.
        Electronics. 8. 668. 10.3390/electronics8060668. 
    '''
    def expand_Q(P, Z):
        '''
        Returns np.array for a given shift value P with lifting Z
        '''
        if P < 0:
            return np.zeros((Z, Z), dtype=int)
        return np.roll(np.eye(Z, dtype=int), P, axis=1)

    # Matrix with shift values
    shift_coeffs = np.loadtxt(filename, delimiter='\t', dtype=int)

    # Only modulus needed
    exponent = np.mod(shift_coeffs, Z, out=shift_coeffs,
                      where=(shift_coeffs > 0))

    # Gathering matrices
    Qs = [[expand_Q(P, Z) for P in row] for row in exponent]
    H = np.block(Qs)
    return H


# 2. Coding.

HParts = namedtuple('HParts',
    ['C', 'D', 'E',
     'A', 'B', 'T',
     'D_C',
    ])

def split_H(H, Z):
    '''
    Splits parity check matrix H into 6 matrices for coding according to
    G. Cisek and T. P. Zieliński, "Prototyping Software Transceiver for
        the 5G New Radio Physical Uplink Shared Channel," 2019 Signal
        Processing Symposium (SPSympo), 2019, pp. 150-155,
        doi: 10.1109/SPS.2019.8882079.
    Z - lifting value.

    H = C D E
        A B T
    '''
    m, n = H.shape
    g = 4*Z

    C = H[:g,            :(n - m)    ]
    D = H[:g, (n - m)    :(n - m + g)]
    E = H[:g, (n - m + g):           ]

    A = H[g:,            :(n - m)    ]
    B = H[g:, (n - m)    :(n - m + g)]
    T = H[g:, (n - m + g):           ]

    D_C = (np.linalg.inv(D) @ C) % 2

    return HParts(C=C, D=D, E=E,
                  A=A, B=B, T=T,
                  D_C=D_C,
                 )

def get_rv_shift(rv, Z):
    '''
    Returns shift value for a giver redundancy version 'rv'
    and lifting size Z.

    Note: only BG1 supported.
    '''
    BG1_RV_SHIFTS = {
        0:  0,
        1: 17,
        2: 33,
        3: 56,
    }
    return BG1_RV_SHIFTS[rv] * Z

def nrz(codeword):
    '''
    Maps 0 to +1, 1 to -1 (BPSK coding).
    '''
    return 1 - codeword*2

def inv_nrz(codeword):
    '''
    Reverse NRZ: restores 0 and 1s (BPSK coding).
    '''
    inv = np.zeros(codeword.shape, dtype=int)
    inv[codeword < 0] = 1
    return inv

# 3. Decoders

def decode_sum_product(r, H, iter_max=20):
    '''
    Tries to decode the received word 'r' from the channel
    using sum-product algorithm.

    Returns pair (decoded, last_codeword_value)
    '''
    def llr(H):
        A = np.log((1 + H)/(1 - H))
        A[np.isnan(A)] = 1
        return A

    H_mirror = (H + np.ones(H.shape)) % 2
    M = r * H
    for i in range(iter_max):
        # Vectorizing, broadcasting...
        M = np.tanh(M / 2.) + H_mirror
        M_rowprod = np.prod(M, axis=1)
        E = M_rowprod.reshape((H.shape[0], -1)) * H / M
        E = llr(E)

        belief = r + np.sum(E, axis=0)
        x = inv_nrz(belief)
        if sum(H @ x % 2) == 0:
            return True, x

        M = H * (belief - E)

    return False, x

def decode_min_sum(r, H, iter_max=20):
    '''
    Tries to decode the received word 'r' from the channel
    using min-sum algorithm.

    Returns pair (decoded, last_codeword_value)
    '''
    H = (H != 0)    # int -> bool
    E = np.empty(H.shape)
    M = r * H
    # Storing top-2 min values
    row_mins = np.empty((M.shape[0], 2))
    for i in range(iter_max):
        for idx, (row, where) in enumerate(zip(M, H)):
            row_mins[idx] = sorted(np.abs(row[where]))[:2]
        row_sign = np.prod(np.sign(M), where=H, axis=1)

        for idx, (col, where) in enumerate(zip(M.T, H.T)):
            # Choosing min value that should not be excluded
            mins = row_mins[:, 0]
            min_mask = (row_mins[:, 0] == col) * where
            mins[min_mask] = row_mins[min_mask, 1]

            E[:, idx] = mins * row_sign * np.sign(col)

        E *= H
        belief = r + np.sum(E, axis=0)
        x = inv_nrz(belief)
        if sum(H @ x % 2) == 0:
            return True, x

        M = H * (belief - E)

    return False, x

# Main classes

class Encoder(object):
    '''
    Contains all the data to encode the data.
    
    Encodes the data with the given matrix H, i.e. prepares the given vector
    for sending to the channel.
    '''

    def __init__(self, H_parts, R, Z, rv, **kwargs):
        '''
        H_parts - H-matrix splitted into parts
        R       - code speed
        Z       - lifting size
        rv      - redundancy version
        kwargs  - just to collect extras...
        '''
        self.L = int(Z * 22 // R)
        self.H = H_parts
        self.Z = Z
        self.rv = rv
        self.rv_shift = get_rv_shift(rv, Z)

    def _code_word(self, s):
        '''
        Returns codeword for 's' using parity check matrix H. See:
        G. Cisek and T. P. Zieliński, "Prototyping Software Transceiver for
            the 5G New Radio Physical Uplink Shared Channel," 2019 Signal
            Processing Symposium (SPSympo), 2019, pp. 150-155,
            doi: 10.1109/SPS.2019.8882079.
        '''
        p1 = (self.H.D_C @ s.T) % 2
        p2 = (self.H.A @ s.T + self.H.B @ p1.T) % 2

        return np.hstack((s, p1, p2))

    def _codeword2tx(self, d):
        '''
        Places codeword 'd' to a circular buffer according to the current
        redundancy version. The length of the resulting tx word is L.
    
        Only BG1 and code rate above 1/3 supported.
        '''
        rv_shift = self.rv_shift
        Z = self.Z
        L = self.L

        start = 2*Z + rv_shift
        if rv_shift + L < len(d[2*Z:]):
            stop = start + L
            return d[start:stop]
        else:   # Note: no overlapping due to small L values
            not_wrapped = d[start:]
            wrapped = d[2*Z:2*Z + L - len(not_wrapped)]
            return np.hstack((not_wrapped, wrapped))
    
    def encode(self, s):
        '''
        Encodes the word 's' (information bits). L bits are sent.
        Modulates the codeword with BPSK.

        Returns coded word.
        '''
        d = self._code_word(s)
        tx = self._codeword2tx(d)
        return d, nrz(tx)

class Channel(object):
    '''
    Acts as a channel :)

    Currently just adds Gaussian moise.
    '''

    def __init__(self, SNR=0.):
        self.SNR = SNR

    def through(self, codeword):
        '''
        Adds Gauss noise with SNR.
        '''
        noise_scale = 10**(-self.SNR / 20)
        e = np.random.normal(scale=noise_scale, size=codeword.shape)

        return codeword + e

class Decoder(object):
    '''
    Contains all the data to decode the data.
    
    Decodes the data with the given matrix H.
    '''

    def __init__(self, H, R, Z, rv, decoder, **kwargs):
        '''
        H       - H-matrix
        R       - code speed
        Z       - lifting size
        rv      - redundancy version
        decoder - algorithm used for decoding
        kwargs  - just to collect extras...
        '''
        self.H = H
        self.Z = Z
        self.rv = rv
        self.rv_shift = get_rv_shift(rv, Z)
        self.decoder = decoder

    def _rx2codeword(self, l):
        '''
        Restores the transmitted codeword from the received word 'l'.
    
        Only BG1 and code rate above 1/3 supported.
        '''
        L = len(l)
        Z = self.Z
        rv_shift = self.rv_shift
    
        d = np.zeros(68 * Z)    # BG1
    
        start = 2*Z + rv_shift
        if rv_shift + L < len(d[2*Z:]):
            stop = start + L
            d[start:stop] = l
        else:   # Note: no overlapping due to small L values
            not_wrap_len = 68*Z - start # BG1
            wrap_len = len(l) - not_wrap_len
            d[2*Z:2*Z + wrap_len] = l[not_wrap_len:]
            d[start:] = l[:not_wrap_len]
    
        return d

    def decode(self, v, iters=40):
        '''
        Decodes the received vector 'v' with the parity matrix H with lifting
        size Z and redundancy vertion rv. 'decoder' is applied.
    
        Returns whether the decoding succeeded and the value of the decoded
        word on the last iteration.
        '''
        d = self._rx2codeword(v)
        # Sort of a hack...
        d = np.where(d != 0, d, np.random.normal(scale=0.1))
        ok, res = self.decoder(d, self.H, iters)
        return ok, res

# Driver

def main():
    def check_R(R):
        R = float(R)
        if R < 1/3:
            raise ArgumentTypeError('Only speed > 1/3 supported')
        if R > 1.:
            raise ArgumentTypeError('Speed must not exceed 1')
        return R

    def get_decoder_for_algo(algo):
        if algo == 'SPA':
            return decode_sum_product
        if algo == 'MSA':
            return decode_min_sum
        raise AttributeError('Unknown decoding algorithm')

    parser = argparse.ArgumentParser(description='LDPC codes in action',
        epilog='With best wishes!')
    parser.add_argument('--snr', default=0, type=float,
        help='Signal-noise ratio at the receiver')
    parser.add_argument('--set', default=1, type=int, choices=[1, 2],
        help='Base Graph version')
    parser.add_argument('--Z',   default=6, type=int,
        help='Lifting size')
    parser.add_argument('--rv',  default=0, type=int, choices=[0, 1, 2, 3],
        help='Redundancy version')
    parser.add_argument('--R',     default=0.35, type=check_R,
        help='Code speed')
    parser.add_argument('--nruns', default=5000, type=int,
        help='Amount of runs of a experiment')
    parser.add_argument('--seed',  default=1984, type=int,
        help='Initial seed value')
    parser.add_argument('--algo',  default='SPA', choices=['SPA', 'MSA'],
        help='Decoding algorithm')
    parser.add_argument('--dst',   default='res', type=str,
        help='Name of the directory for the results')

    # Some additional checks
    args = parser.parse_args()
    decoder = get_decoder_for_algo(args.algo)
    if not os.path.exists(args.dst):
        os.mkdir(args.dst)
    # TODO: check for Z values

    # Read and prepare the parity matrix
    H = read_H(os.path.join('matrices', f'R1-1711982_BG1_set{args.set}.csv'),
        args.Z)
    H_parts = split_H(H, args.Z)
    m, n = H.shape

    enc = Encoder(H_parts=H_parts, **vars(args))
    dec = Decoder(H=H, decoder=decoder, **vars(args))
    chan = Channel(args.snr)
    
    # Run!
    fmtstr = '{seed},{ok},{BER_full:.5},{BER_data:.5}\n'
    outfile = os.path.join(args.dst,
        f'SNR={args.snr:.5}_set={args.set}_Z={args.Z}_rv={args.rv}_'
        f'R={args.R:.5}_alg={args.algo}.dat')
    with open(outfile, 'a') as f:
        for seed in range(args.seed, args.seed + args.nruns):
            np.random.seed(seed)
            s = np.random.choice(2, n - m)

            txed_data, v = enc.encode(s)
            v = chan.through(v)
            ok, got_data = dec.decode(v)
            rx = got_data[:(n - m)]

            f.write(fmtstr.format(
                seed=seed,
                ok=ok,
                BER_full=sum(np.not_equal(got_data, txed_data))/len(txed_data),
                BER_data=sum(np.not_equal(rx, s))/len(s),
                ))

if __name__ == '__main__':
    main()

