#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os.path
import argparse

parser.add_argument('--src', default='res', type=str,
    help='Name of the directory with the results')
parser.add_argument('--dst', default='plot.png', type=str,
    help='Name of the output file')

args = parser.parse_args()

Z = 6
R = 0.35
SNR = [0.1 * x for x in range(-20, 41, 5)]

def get_ber(alg):
    '''
    Calculates bit error rates for a given algorithm 'alg' for all the
    SNRs.

    Returns dict{ rv: [BER]}. BER[i] correspongs to SNR[i].
    '''
    points = {
        0: [],
        1: [],
        2: [],
        3: [],
    }
    for snr in SNR:
        for rv in [0, 1, 2, 3]:
            snr = round(snr, 5)
            filename = os.path.join(f'{args.src}',
                f'SNR={snr}_set=1_Z={Z}_rv={rv}_R={R}_alg={alg}.dat')
            df = pd.read_csv(filename,
                             names=['seed', 'success', 'BER_full', 'BER'])
            BER = df['BER'].mean()
            points[rv].append(BER)

    return points
    

points = get_ber('SPA')
for rv, BER in points.items():
    plt.plot(SNR, BER, label=f'SPA, Z={Z}, R={R}, rv={rv}')

plt.grid()
plt.xlabel('SNR')
plt.ylabel('BER')
plt.yscale('log')
plt.legend(loc='best')
plt.savefig('readme.png')

