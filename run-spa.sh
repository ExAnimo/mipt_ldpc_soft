#!/bin/bash

runs=1000
rng=1

for R in 0.35 ; do
    for Z in 6 ; do
        for SNR in $(seq -2 0.5 4); do
        # Running 4 tasks simultaneously
        # TODO: add better parallel logic
            for rv in 0 1 2 3 ; do
                nohup ./main.py --snr=$SNR --set=1 --Z=$Z --rv=$rv --R=$R \
                    --nruns=$runs --seed=$rng --algo=SPA --dst=asd &>/dev/null &
            done

            wait
        done
    done
done

