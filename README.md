# MIPT_LDPC_Soft

Requires Python3.6+

An implementation of a soft decoder for LDPC codes.
Currently 2 algorithms are supported: Sum-Product (SPA) and Min-Sum (MSA).

## Example

Here is an exapmle plot for LDPS with BPSK modulation obtained with the
simulation:

![BER/SNR](readme.png)

## How to run the experiment

To run the experiment from the example on your Linux machine you can just use
the prepared script (the simulation may take several hours):
```bash
$ ./run-msa.sh &
```
To change the default values to the desired ones, just open the first script and
alter the command line.
After the script finished its execution, run the plotter script:
```bash
$ ./plotter.py
```

After that a picture will appear in your working directory.

## Interface

For more sophisticated operations you may find it useful to write your own
scripts rather than use `run-spa.sh` and `plotter.py`.

To check available parameters of `main.py` just run
```bash
$ ./main.py -h
```

